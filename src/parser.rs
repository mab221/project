// Here is where the various combinators are imported. You can find all the combinators here:
// https://docs.rs/nom/5.0.1/nom/
// If you want to use it in your parser, you need to import it here. I've already imported a couple.

use nom::{
  IResult,
  branch::alt,
  combinator::recognize,
  multi::{many1, many0, separated_list},
  bytes::complete::tag,
  character::complete::{alphanumeric1, digit1, space1 },
};

// Here are the different node types. You will use these to make your parser and your grammar.
// You may add other nodes as you see fit, but these are expected by the runtime.

#[derive(Debug, Clone)]
pub enum Node {
  Program { children: Vec<Node> },
  Statement { children: Vec<Node> },
  FunctionReturn { children: Vec<Node> },
  FunctionDefine { children: Vec<Node> },
  FunctionArguments { children: Vec<Node> },
  FunctionStatements { children: Vec<Node> },
  Expression { children: Vec<Node> },
  MathExpression {name: String, children: Vec<Node> },
  FunctionCall { name: String, children: Vec<Node> },
  VariableDefine { children: Vec<Node> },
  Number { value: i32 },
  Bool { value: bool },
  Identifier { value: String },
  String { value: String },
  Null{},
}

// Define production rules for an identifier
pub fn identifier(input: &str) -> IResult<&str, Node> {
  let (input, res) = alphanumeric1(input)?;              // Consume at least 1 alphanumeric character. The ? automatically unwraps the result if it's okay and bails if it is an error.
  Ok((input, Node::Identifier{ value: res.to_string()})) // Return the now partially consumed input, as well as a node with the string on it.
}

// Define an integer number
pub fn number(input: &str) -> IResult<&str, Node> {
  let (input, res) = digit1(input)?;                     // Consume at least 1 digit 0-9
  let num = res.parse::<i32>().unwrap();              // Parse the string result into a usize
  Ok((input, Node::Number{ value: num}))                 // Return the now partially consumed input with a number as well
}

pub fn boolean(input: &str) -> IResult<&str, Node> {
  let(input, res) = alt((tag("true"), tag("false")))(input)?;
  let bool_val = if res == "true" {true} else {false};
  Ok((input, Node::Bool{ value: bool_val}))
}

pub fn string(input: &str) -> IResult<&str, Node> {
  let(input, _) = tag("\"")(input)?;
  let(input, res) = many1(alt((alphanumeric1, space1)))(input)?;
  let(input, _) = tag("\"")(input)?;
  Ok((input, Node::String{value: res.join("")}))
}

pub fn function_call(input: &str) -> IResult<&str, Node> {
  let(input, res) = identifier(input)?;
  let name: String = match res{
    Node::Identifier{value} => value.clone(),
    _=>"".to_string(),
  };
  let(input, _) = tag("(")(input)?;
  let(input, _) = many0(space1)(input)?;
  let(input, ar) = arguments(input)?;
  let(input, _) = many0(space1)(input)?;
  let(input, _) = tag(")")(input)?;
 
  Ok((input, Node::FunctionCall{name: name.to_string() , children:  vec![ar] }))
}

// Math expressions with parens (1 * (2 + 3))
pub fn parenthetical_expression(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("(")(input)?;
  let(input, res) = math_expression(input)?;
  let (input, _) = tag(")")(input)?;
  Ok((input, Node::Expression{children: vec![res]}))
}

pub fn l4(input: &str) -> IResult<&str, Node> {
  alt((function_call, number, identifier, parenthetical_expression))(input)
}

pub fn l3_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, result) = tag("^")(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l4(input)?;
  Ok((input, Node::MathExpression{name: result.to_string(), children: vec![args]}))

}

pub fn l3(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l4(input)?;
  let (input, tail) = many0(l3_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))

}

pub fn l2_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, result) = alt((tag("*"),tag("/")))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l3(input)?;
  Ok((input, Node::MathExpression{name: result.to_string(), children: vec![args]}))

}

pub fn l2(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l3(input)?;
  let (input, tail) = many0(l2_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))


}

// L1 - L4 handle order of operations for math expressions 
pub fn l1_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, result) = alt((tag("+"),tag("-")))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l2(input)?;
  Ok((input, Node::MathExpression{name: result.to_string(), children: vec![args]}))

}

pub fn l1(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l2(input)?;
  let (input, tail) = many0(l1_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))
}

pub fn math_expression(input: &str) -> IResult<&str, Node> {
  l1(input)
}

pub fn expression(input: &str) -> IResult<&str, Node> {
  let (input, expr) = alt(( boolean, function_call, math_expression, number, string, identifier))(input)?;
  Ok((input, Node::Expression{children:vec![expr]}))

}

pub fn statement(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(alt((tag(" "), tag("\n"))))(input)?;
  let(input, statement) = variable_define(input)?;
  let(input, _) = tag(";")(input)?;
  Ok((input, Node::Statement{children: vec![statement]}))
}

pub fn function_return(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(alt((tag(" "), tag("\n"))))(input)?;
  let(input, _) = tag("return ")(input)?;
  let(input, _) = many0(space1)(input)?;
  let(input, fn_expr) = expression(input)?;
  let(input, _) = tag(";")(input)?;
  Ok((input, Node::FunctionReturn{children:vec![fn_expr]}))
}

// Define a statement of the form
// let x = expression
pub fn variable_define(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("let ")(input)?;
  let (input, var) = identifier(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, _) = tag("=")(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, expr) = expression(input)?;
  Ok((input, Node::VariableDefine{ children: vec![var, expr]}))   
}

pub fn arguments(input: &str) -> IResult<&str, Node> {
  let(input, ar) = separated_list(tag(","), expression)(input)?;
  Ok((input, Node::FunctionArguments{children:  ar }))
}

pub fn function_definition(input: &str) -> IResult<&str, Node> {

  let mut v = Vec::new();
  let (input, _) = tag("fn ")(input)?;
  let (input, name) = identifier(input)?;
  let (input, _) = tag("(")(input)?;
  let (input, ar) = arguments(input)?;
  let (input, _) = tag(")")(input)?;
  let (input, _) = many0(alt((tag(" "), tag("\n"))))(input)?;
  let (input, _) = tag("{")(input)?;
  let (input, _) = many0(alt((tag(" "), tag("\n"))))(input)?;
  let (input, mut stats) = many0(statement)(input)?;
  let (input, _) = many0(alt((tag(" "), tag("\n"))))(input)?;
  let (input, fn_return) = function_return(input)?;
  let (input, _) = many0(alt((tag(" "), tag("\n"))))(input)?;
  let (input, _) = tag("}")(input)?; 
  let (input, _) = many0(alt((tag(" "), tag("\n"))))(input)?;
  
  v.push(name);
  v.push(ar);
  v.append(&mut stats);
  v.push(fn_return);
  let (input, _) = many0(alt((tag(" "), tag("\n"))))(input)?;
  Ok((input, Node::FunctionDefine { children: v })) 
 
}

pub fn comment(input: &str) -> IResult<&str, Node> {
  unimplemented!();
}

// Define a program. You will change this, this is just here for example.
// You'll probably want to modify this by changing it to be that a program
// is defined as at least one function definition, but maybe more. Start
// by looking up the many1() combinator and that should get you started.
pub fn program(input: &str) -> IResult<&str, Node> {
  let (input, res) = many1(alt((function_definition , statement, expression)))(input)?; // Now that we've defined a number and an identifier, we can compose them using more combinators. Here we use the "alt" combinator to propose a choice.
  //Ok((input, Node::Program{ children: vec![result]}))       // Whether the result is an identifier or a number, we attach that to the program
  Ok((input, Node::Program{ children: res}))

}
